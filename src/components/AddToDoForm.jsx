import React, { useState } from 'react';
const AddTodoForm = ({ addTodo }) => {
  const [text, setText] = useState('');
  const handleSubmit = (e) => {
    e.preventDefault();
    if (!text.trim()) return;
    addTodo(text);
    setText('');
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
        placeholder="Write here"/>
      <button type="submit">Add to list</button>
    </form>
  );};
export default AddTodoForm;