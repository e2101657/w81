import React, { useState } from 'react';
import TodoList from './ToDoList';
import AddTodoForm from './AddToDoForm';
import Menu from '../menu/Menu';
import './App.css';

const App = () => {
  const [todos, setTodos] = useState([]);
  const addTodo = (text) => {
    setTodos([...todos, { text, completed: false }]);
  };
  return (
    <div>
      <Menu /> {}
      <h1>To-do List</h1>
      <TodoList todos={todos} />
      <AddTodoForm addTodo={addTodo} />
    </div>
  );};
export default App;